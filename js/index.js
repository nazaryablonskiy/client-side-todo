var callsDb = new localStorageDB('callsDb', localStorage);

if ( !callsDb.tableExists("calls") ) {
  callsDb.createTable("calls", ["name", "phone_number", "time", "done"]);
  callsDb.commit();
}