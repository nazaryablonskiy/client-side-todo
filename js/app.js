var callsDb = new localStorageDB('callsDb', localStorage);
var app = angular.module('myApp', []);
app.controller('myCtrl', ['$scope', '$log', function($scope, $log) {
  $scope.orderByField = 'time';
  $scope.reverseSort = false;
  // #event handlers
  $scope.insertCall = function() {
    if ($scope.name && $scope.tel && $scope.time) {
      var currentTime = new Date()
      var time = new Date($scope.time);
      currentTime.setHours(time.getHours());
      currentTime.setMinutes(time.getMinutes());
      var callObj = {
        name: $scope.name,
        phone_number: $scope.tel,
        time: currentTime,
        done: false
      };
      callsDb.insert("calls", callObj);
      callsDb.commit();
      clearForm();
      fetchCalls({});
    }
  };

  $scope.allCalls = function(){
    fetchCalls({});
  }

  $scope.finishedCalls = function(){
    fetchCalls({done: true});
  }

  $scope.nextCalls = function(){
    fetchCalls({done: false});
  }

  $scope.deleteCall = function(ID){
    callsDb.deleteRows("calls", {ID: ID});
    callsDb.commit();
    fetchCalls();
  }

  $scope.orderBy = function(fieldName){
    if ($scope.orderByField === fieldName) {
      $scope.reverseSort = !$scope.reverseSort
    }
    $scope.orderByField = fieldName;
    fetchCalls({});
  }
  // /event handlers

  fetchCalls = function(query){
    var sortingType = $scope.reverseSort ? 'DESC' : 'ASC';
    $scope.calls = callsDb.queryAll('calls', {
      query: query,
      sort: [[$scope.orderByField, sortingType]]
    }).map(function(call){
      call.done = isFinished(call.time);
      call.time = timeStringToTime(call.time);
      callsDb.update("calls", {ID: call.ID}, function(callDoc){
        callDoc.done = call.done;
        return callDoc;
      });
      return call;
    });
    fetchNextCall();
  }

  clearForm = function(){
    $scope.name = '';
    $scope.tel = '';
    $scope.time = '';
  }

  timeStringToTime = function(timeStr) {
    var callTime = new Date(timeStr)
    return callTime.toTimeString().slice(0,5);
  }

  fetchNextCall = function(){
    nextCall = callsDb.queryAll('calls', {
      query: {
        done: false
      },
      sort: 'time',
      limit: 1
    })[0];
    if (nextCall) {
      nextCall.time = timeStringToTime(nextCall.time);
      $scope.nextCall = nextCall;
    }
  }

  isFinished = function(time){
    return new Date() > new Date(time);
  }

  fetchCalls();
}]);